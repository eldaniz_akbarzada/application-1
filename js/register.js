function Person(name,surname,email,password){
    this.name=name;
    this.surname=surname;
    this.email=email;
    this.password=password;
}
document.addEventListener("DOMContentLoaded",function(){
    document.getElementById("submit").addEventListener("click",function(r){
        r.preventDefault();
        let name=document.getElementById("name_input").value;
        let surname=document.getElementById("surname_input").value;
        let email=document.getElementById("email_input").value;
        let password=document.getElementById("password_input").value;

        let person= new Person(name,surname,email,password);


        if("people" in localStorage){
          AllPeople=JSON.parse(localStorage.people);
          if("length" in AllPeople){
            AllPeople.push(person);
            localStorage.people=JSON.stringify(AllPeople);

          }
          else{
              mas=[];
              mas.push(AllPeople);
              mas.push(person);
              localStorage.people=JSON.stringify(mas);
          }

        }
        else{
            localStorage.people=JSON.stringify(person);
        }

    })

})